# onyxz7/ansible-homelab

An Ansible playbook that sets up an Fedora-Server with some cool services. Act mainly as a media server for nom. See ToDoMap for more info

It assumes a fresh Fedora Server installation, access to a non-root user with sudo privileges and a public SSH key. This can be configured during the installation process. It also assumes you use duckdns as domain provider and surfshark as VPN.
For storage, the default is setup for 3 disks to setup a raid 5, but you can change it in group_vars/all/vars.yml the raid level and disks added

The playbook is mostly being developed for personal use, so stuff is going to be constantly changing and breaking. Use at your own risk and don't expect any help in setting it up on your machine.

## Installation
Go see [INSTALL.md](https://gitlab.com/onyxz7/ansible-homelab/-/blob/main/install/INSTALL.md) 

## Special thanks
* David Stephens for his [Ansible NAS](https://github.com/davestephens/ansible-nas) project.
* Wolfgang (notthebee) for his [Infra](https://github.com/notthebee/infra) project. This is where I got the idea and "borrowed" a lot of concepts and implementations from.
* Larry Smith Jr. for his [ansible-mdadm](https://github.com/mrlesmithjr/ansible-mdadm) role.

## Services included:
* [Cockpit](https://cockpit-project.org/) 
* [Homepage](https://github.com/benphelps/homepage) 
* [Prowlarr](https://prowlarr.com/) 
* [Radarr](https://radarr.video/) 
* [Sonarr](https://sonarr.tv/) 
* [Lidarr](https://lidarr.audio/) 
* [Readarr](https://readarr.com/) 
* [Flaresolverr](https://github.com/FlareSolverr/FlareSolverr) 
* [Transmission](https://transmissionbt.com/) 
* [Traefik Proxy](https://traefik.io/traefik/) 
* [Portainer](https://www.portainer.io/)
* [DuckDns](https://hub.docker.com/r/linuxserver/duckdns)
* [Gluetun](https://github.com/qdm12/gluetun)
* [Watchtower](https://containrrr.dev/watchtower/)

## Archive Mention
I now have a more complex homelab. Even if this project still usefull if you only have 1 node, I will not improve this project more than it is already.