# SSH Key authentication
If you are there, it's because you want to know how how authenticate in a ssh server with a ssh key.

## Local VS Password Manager
I personnaly use 1password as password manager, and the ssh agent is perfect for this use.
After following [this doc](https://developer.1password.com/docs/ssh/), I am now able to not specify password or key file, and 1password detect when and what is needed.
Because it's not everyone that have a password manager with ssh agent, I will explain how do it localy.
The amin issue of the local method, is that private key will be in clear. There is some ways to protect it, but it's nothing like put it in a password manager.

## Setup the SSH key

### Generate private key
    ssh-keygen -o -a 100 -t ed25519 -f ~/.ssh/nodes -C <server-username>@<server-domain>

### Send the public key to the remote server
    ssh-copy-id -i ~/.ssh/nodes <server-username>@<ip-or-domain>

### Test log-in ssh
    ssh -i ~/.ssh/nodes <server-username>@<ip-or-domain>

## Hosts file for local SSH key
If you use the local SSH key method, you will need to uncomment the last line about ssh secret file. 
Verify also that the path to it is correct
