# Installation

Ansible Homelab is an Ansible playbook that sets up services on your home server.

## Requirements

* A Linux environment with Ansible installed (in Ansible terms the "control node"). See the official Installing Ansible docs for more info.
* SSH access to a fresh Fedora server that'll become your Ansible Homelab box.
* Private key authentication for accessing the remote hosts (see [ssh_auth.md](https://gitlab.com/onyxz7/ansible-homelab/-/blob/main/install/ssh_auth.md))

## Running Ansible Homelab

### Linux
1. Clone Ansible Homelab repository and go in it

    `git clone https://gitlab.com/onyxz7/ansible-homelab.git && cd ansible-homelab`

2. Edit the hosts file for including your nodes.

    `$EDITOR hosts`

3. Edit the vars file with your own configuration

   `$EDITOR group_vars/all/vars.yml`

3. Edit the secret file with your own passwords and tokens

    `ansible-vault edit group_vars/all/secrets.yml`

4. Run the playbook for the first time (become mean enter sudo password)

    `ansible-playbook run.yml -K --ask-vault-pass`

5. Run the playbook every other times

    `ansible-playbook run.yml --ask-vault-pass`
